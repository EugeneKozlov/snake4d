﻿module Snake4D.util;

import std.algorithm;
import std.math;

import gl3n.linalg;

alias Int4x4 = Matrix!(int, 4, 4);
alias Int4 = Vector!(int, 4);
alias Float3x3 = Matrix!(float, 3, 3);
alias Float4x4 = Matrix!(float, 4, 4);
alias Float3 = Vector!(float, 3);
alias Float4 = Vector!(float, 4);
alias Quat = Quaternion!(float);

Value lerp(Value, Factor)(in Value left, in Value right, in Factor factor)
{
	return left * (1 - factor) + right * factor;
}

Float4 mul(in Float4 lhs, in Float4 rhs)
{
	return Float4(lhs.x*rhs.x, lhs.y*rhs.y, lhs.z*rhs.z, lhs.w*rhs.w);
}

Int4 mul(in Int4 lhs, in int rhs)
{
	return Int4(lhs.x*rhs, lhs.y*rhs, lhs.z*rhs, lhs.w*rhs);
}

Int4 div(in Int4 lhs, in int rhs)
{
	return Int4(lhs.x/rhs, lhs.y/rhs, lhs.z/rhs, lhs.w/rhs);
}

bool inside(in Int4 value, in Int4 min, in Int4 max)
{
	return min.x <= value.x && value.x <= max.x 
		&& min.y <= value.y && value.y <= max.y 
		&& min.z <= value.z && value.z <= max.z
		&& min.w <= value.w && value.w <= max.w;
}

uint asUint(in Float3 color)
{
	int fr = cast(int) round(color.x*255.0f);
	int fg = cast(int) round(color.y*255.0f);
	int fb = cast(int) round(color.z*255.0f);
	ubyte r = cast(ubyte) clamp(fr, 0, 255);
	ubyte g = cast(ubyte) clamp(fg, 0, 255);
	ubyte b = cast(ubyte) clamp(fb, 0, 255);
	return 0xff000000 | r | (g << 8) | (b << 16);
}

Float3 doppler(in Float3 src, in float factor)
{
	if (factor < 0)
		return lerp(src, Float3(1, src.y/4, src.y/4), -factor);
	else
		return lerp(src, Float3(src.x/4, src.y/3, 1), factor);
}

Quat slerp(in Quat lhs, in Quat rhs, in float factor)
{
	// calc cosine theta
	float cosom = lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z + lhs.w * rhs.w;
	// adjust signs (if necessary)
	Quat end = rhs;
	if (cosom < 0.0f)
	{
		cosom = -cosom;
		end.x = -end.x;   // Reverse all signs
		end.y = -end.y;
		end.z = -end.z;
		end.w = -end.w;
	} 
	// Calculate coefficients
	float sclp, sclq;
	if ((1.0f - cosom) > 0.0001f) // 0.0001 -> some epsillon
	{
		// Standard case (slerp)
		float omega, sinom;
		omega = acos( cosom); // extract theta from dot product's cos theta
		sinom = sin( omega);
		sclp  = sin( (1.0f - factor) * omega) / sinom;
		sclq  = sin( factor * omega) / sinom;
	} 
	else
	{
		// lerp
		sclp = 1.0f - factor;
		sclq = factor;
	}
	return Quat(sclp * lhs.w + sclq * end.w,
				sclp * lhs.x + sclq * end.x, 
				sclp * lhs.y + sclq * end.y,
				sclp * lhs.z + sclq * end.z);
}

Int4 getAxis(in Int4x4 matrix, in uint axis)
{
	return Int4(matrix[0][axis], matrix[1][axis], matrix[2][axis], matrix[3][axis]);
}

void setAxis(out Int4x4 matrix, in uint axis, in Int4 value)
{
	matrix[0][axis] = value.x;
	matrix[1][axis] = value.y;
	matrix[2][axis] = value.z;
	matrix[3][axis] = value.w;
}

Quat quatByMatrix(in Int4x4 mat)
{
	Float3x3 m;
	foreach (uint i; 0 .. 3)
		foreach (uint j; 0 .. 3)
		{
			m[i][j] = cast(float) mat[j][i];
		}
	return Quat.from_matrix(m);
}

Float4x4 lerpRotation(in Int4x4 lhs, in Int4x4 rhs, float factor)
{
	Quat lq = quatByMatrix(lhs);
	Quat rq = quatByMatrix(rhs);
	Quat qq = slerp(lq, rq, factor);
	Float4x4 m = qq.to_matrix!(3, 3)();
	return m;
}

Float4x4 castMatrix(in Int4x4 src)
{
	Float4x4 dest;
	foreach(uint r; 0 .. 4) 
		foreach(uint c; 0 .. 4) 
			dest[r][c] = cast(float) src[r][c];
	return dest;
}

Float4 castVector(in Int4 src)
{
	Float4 dest;
	foreach(uint c; 0 .. 4) 
		dest.vector[c] = cast(float) src.vector[c];
	return dest;
}
