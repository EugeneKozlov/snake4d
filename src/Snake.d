module Snake4D.Snake;

import std.random;
import std.algorithm;
import std.math;

import derelict.opengl3.gl;

import Snake4D.util;
import Snake4D.World4D;
import Snake4D.TimeManager;

class Snake
{
	enum Nothing = 0;
	enum Head = 1;
	enum Food = -1;
	enum HeadColor = Float3(0.8, 0.8, 0.0);
	enum FoodColor = Float3(0.0, 1.0, 0.0);

	private bool inside(Int4 v)
	{
		return Snake4D.util.inside(head, Int4(0), Int4(cast(int) size - 1));
	}
	private ref int at(Int4 v)
	{
		return map[v.x][v.y][v.z][v.w];
	}

	public this(in uint size, in uint initLength, in uint foodRichness, 
				in double initSpeed, in double acceleration)
	{
		this.size = size;
		this.head = Int4(0, 0, 0, size/2);
		this.length = initLength;
		this.initLength = initLength;
		this.map = new int[][][][](size, size, size, size);
		this.foodRichness = foodRichness;
		this.speed = initSpeed;
		this.acceleration = acceleration;
		at(head) = Head;

		this.world = new World4D(size, min(size/2, 6), head);
		auto tick = delegate(in double speed)
		{
			act(action);
			move();
			action = Action.Move;
			return this.speed;
		};
		this.timer = new TimeManager(tick, initSpeed);

		throwFood();
	}
	public void addDelayedAction(in Action action) nothrow
	{
		this.action = action;
	}
	private void act(in Action action)
	{
		final switch (action)
		{
			case Action.No:
				world.stand();
				break;
			case Action.Move:
				world.move();
				break;
			case Action.Down:
				world.rotateXW(false);
				break;
			case Action.Up:
				world.rotateXW(true);
				break;
			case Action.Left:
				world.rotateYW(false);
				break;
			case Action.Right:
				world.rotateYW(true);
				break;
			case Action.Red:
				world.rotateXY(false);
				break;
			case Action.Blue:
				world.rotateXY(true);
				break;
			case Action.Horize:
				world.mirrorXW();
				break;
			case Action.Vertize:
				world.mirrorYW();
				break;
			case Action.Future:
				break;
			case Action.Past:
				break;
		}
		head = world.cameraPosition();
	}
	private void move() 
	{
		// test head
		if (!inside(head) || at(head) > Head)
		{
			dead = true;
			return;
		}

		if (at(head) == Food)
		{
			speed /= acceleration;
			length += foodRichness;
			at(head) = Nothing;
			throwFood();
		}

		// move
		foreach (i; 0 .. size)
			foreach (j; 0 .. size)
				foreach (k; 0 .. size)
					foreach (l; 0 .. size)
					{
						Int4 v = Int4(i, j, k, l);
						if (at(v) > 0)
							++at(v);
						if (at(v) > cast(int) length)
							at(v) = Nothing;
					}
		at(head) = Head;
	}
	private void throwFood()
	{
		bool volume = head != Int4(0, 0, 0, size/2);
		bool success = false;
		do
		{
			food = Int4(uniform(0, size), uniform(0, size), uniform(0, size), volume ? uniform(0, size) : size/2);
			if (at(food) == 0)
			{
				at(food) = Food;
				success = true;
			}
		} while(!success);
	}

	public bool update(in double deltaTime)
	{
		timer.updateTime(deltaTime);
		return !dead;
	}
	private void drawUi(in float LineHeigth, in float HeadSize, in float FoodSize, in float ratio)
	{
		Int4 axisW = world.colorDirection();
		int headW = axisW * head, foodW = axisW * food;
		if (axisW.x > 0 || axisW.y > 0 || axisW.z > 0 || axisW.w > 0)
		{
		}
		else
		{
			headW = size + headW - 1;
			foodW = size + foodW - 1;
		}

		glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

		glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
		glDisable(GL_DEPTH_TEST);

		glBegin(GL_QUADS);
		{
			// red
			glColor3f(1, 0, 0);
			glVertex2f(-1, 1);
			glVertex2f(-1, 1 - LineHeigth);
			glColor3f(1, 1, 1);
			glVertex2f(0, 1 - LineHeigth);
			glVertex2f(0, 1);
			// blue
			glColor3f(1, 1, 1);
			glVertex2f(0, 1 - LineHeigth);
			glVertex2f(0, 1);
			glColor3f(0, 0, 1);
			glVertex2f(1, 1);
			glVertex2f(1, 1 - LineHeigth);
			// head
			glColor3fv(HeadColor.value_ptr);
			float headx = (headW + 0.5f) / size * 2 - 1;
			float heady = 1 - LineHeigth/2;
			float headh = LineHeigth / 2 * HeadSize;
			float headw = headh / ratio;
			glVertex2f(headx - headw, heady + headh);
			glVertex2f(headx - headw, heady - headh);
			glVertex2f(headx + headw, heady - headh);
			glVertex2f(headx + headw, heady + headh);
			// food
			glColor3fv(FoodColor.value_ptr);
			float foodx = (foodW + 0.5f) / size * 2 - 1;
			float foody = 1 - LineHeigth/2;
			float foodh = LineHeigth / 2 * FoodSize;
			float foodw = foodh / ratio;
			glVertex2f(foodx - foodw, foody);
			glVertex2f(foodx, foody - foodh);
			glVertex2f(foodx + foodw, foody);
			glVertex2f(foodx, foody + foodh);
		}
		glEnd();
	}
	public void draw(in float fovy, in float zfar, in float znear, in float ratio)
	{
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);

		// camera
		world.loadCamera(timer.getLerp(), fovy, zfar, znear, ratio, 5.0f, PI / 6);

		// draw world
		glLineWidth(2);
		world.drawRemaning(Float3(1.0f, 1.0f, 1.0f), 0.08f);
		foreach (i; 0 .. size)
			foreach (j; 0 .. size)
				foreach (k; 0 .. size)
					foreach (l; 0 .. size)
					{
						Int4 v = Int4(i, j, k, l);
						switch (at(v))
						{
							case Nothing:
								break;
							case Head:
								world.addCube(v, 0.75f, 0.75f, HeadColor);
								break;
							case Food:
								world.addStar(v, 0.55f, 0.95f, FoodColor);
								break;
							default:
								world.addCube(v, 0.55f, 0.95f, Float3(1.0f, 1.0f, 1.0f));
								break;
						}
					}
		world.drawFigures();

		// draw etc
		glLineWidth(1);
		world.drawGrid(Float3(0.7f, 0.7f, 0.7f));

		drawUi(0.1f, 2.0f/3.0f, 1.0f/2.0f, ratio);
	}

	public uint score()
	{
		return (length - initLength) / foodRichness;
	}

	private bool dead = false;
	private double acceleration;
	private double speed;
	private uint size;
	private uint foodRichness;
	private Int4 head;
	private Int4 food;
	private uint length;
	private uint initLength;
	private int[][][][] map;
	private World4D world;
	private TimeManager timer;
	private Action action = Action.Move;
}