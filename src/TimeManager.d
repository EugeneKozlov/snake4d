﻿module Snake4D.TimeManager;

class TimeManager
{
public:
	alias TickFunction = double delegate(in double speed);
	this(TickFunction onTick, double initialSpeed)
	{
		this.onTick = onTick;
		this.speed = initialSpeed;
	}
	void updateTime(double deltaTime)
	{
		delta += deltaTime;
		while (delta * speed > 1)
		{
			double remaning = 1 / speed;
			delta -= remaning;
			time += remaning;
			speed = onTick(speed);
		}
	}
	double getLerp()
	{
		return delta * speed;
	}

private:
	TickFunction onTick;
	double delta = 0;
	double time = 0;
	double speed;
}

