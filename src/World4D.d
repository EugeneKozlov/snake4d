﻿module Snake4D.World4D;

import std.stdio;
import std.math;

import derelict.opengl3.gl;
import gl3n.linalg;

import Snake4D.util;

enum Action
{
	No,
	Move,
	Down,
	Up,
	Left,
	Right,
	Red,
	Blue,
	Horize,
	Vertize,
	Past,
	Future
}

class World4D
{
	private enum MaxVertices = 8 * 1024;
	private enum MaxIndices = 24 * 1024;
	private enum H = 0.5f;

	private enum X = Int4.e1;
	private enum Y = Int4.e2;
	private enum Z = Int4.e3;
	private enum W = Int4.e4;

	public this(uint gridSize, uint maxW, Int4 startPosition)
	{
		this.gridSize = gridSize;
		this.maxW = maxW;
		first.reset(startPosition);
		second.reset(startPosition);
	}

	private void update(in Int4x4 modify, in Int4x4 rotate, in bool fast, in bool move)
	{
		// update 4D
		first.poseMatrix = second.poseMatrix;
		first.position = second.position;

		second.poseMatrix = first.poseMatrix * modify;
		if (move)
			second.position = first.position + moveDirection();

		// update 3D rotation
		first.renderRotation = second.renderRotation;
		second.renderRotation = first.renderRotation * rotate;

		// remap
		first.remap = second.remap;
		second.remap = second.poseMatrix * second.renderRotation.inverse;

		// reset
		factor = 0;
		factorCameraPosition = 0;
		factorObject = 0;
		useFastForPosition = fast;

		// log
		writeln("Moved to ", second.position);
	}
	public void stand()
	{
		update(Int4x4.identity, Int4x4.identity, false, false);
	}
	public void move()
	{
		update(Int4x4.identity, Int4x4.identity, false, true);
	}
	public void rotateXW(bool up)
	{
		static immutable Int4x4 upMatrix = Int4x4(X, -Z, Y, W).transposed();
		static immutable Int4x4 downMatrix = Int4x4(X, Z, -Y, W).transposed();
		Int4x4 matrix = up ? upMatrix : downMatrix;

		update(matrix, matrix, false, true);
	}
	public void rotateYW(bool right)
	{
		static immutable Int4x4 rightMatrix = Int4x4(-Z, Y, X, W).transposed();
		static immutable Int4x4 leftMatrix = Int4x4(Z, Y, -X, W).transposed();
		Int4x4 matrix = right ? rightMatrix : leftMatrix;

		update(matrix, matrix, false, true);
	}
	public void rotateZW(bool clockwise)
	{
		static immutable Int4x4 cwMatrix = Int4x4(-Y, X, Z, W).transposed();
		static immutable Int4x4 ccwMatrix = Int4x4(Y, -X, Z, W).transposed();
		Int4x4 matrix = clockwise ? cwMatrix : ccwMatrix;

		update(matrix, matrix, false, true);
	}
	public void rotateXY(bool blue)
	{
		static immutable Int4x4 redMatrix = Int4x4(X, Y, -W, Z).transposed();
		static immutable Int4x4 blueMatrix = Int4x4(X, Y, W, -Z).transposed();
		Int4x4 matrix = blue ? blueMatrix : redMatrix;

		update(matrix, Int4x4.identity, true, true);
	}
	public void mirrorXW()
	{
		static immutable Int4x4 matrix = Int4x4(W, Y, Z, X).transposed();

		update(matrix, Int4x4.identity, false, true);
	}
	public void mirrorYW()
	{
		static immutable Int4x4 matrix = Int4x4(X, W, Z, Y).transposed();

		update(matrix, Int4x4.identity, false, true);
	}

	private Float4 remapPosition(in Int4 position, in Int4x4 remapMatrix)
	{
		Int4 remapped = position * remapMatrix;
		Int4 eye = Int4(1, 1, 1, 1) * remapMatrix - Int4(1);
		Int4 offset = mul(div(eye, -2), cast(int) (gridSize - 1));
		return castVector(remapped + offset);
	}
	private Float4 remapPosition(in Int4 firstPosition, in Int4 secondPosition, in float factor)
	{
		Float4 firstRemapped = remapPosition(firstPosition, first.remap);
		Float4 secondRemapped = remapPosition(secondPosition, second.remap);
		return lerp(firstRemapped, secondRemapped, factor);
	}
	public void loadCamera(in double cameraLerp, in float fovy, in float zfar, in float znear, in float ratio, in float distance, in float angle)
	{
		factor = cameraLerp;
		factorCameraPosition = cameraLerp ^^ 0.25f;
		factorObject = clamp(cameraLerp * 3/2, 0, 1) ^^ 0.25f;

		// projection
		float f = 1 / tan(fovy / 2);
		float a = zfar / (zfar - znear);
		float b = znear * zfar / (zfar - znear);
		float[4][4] projectionMatrix = 
		[
			[ f / ratio,0,		0,		0],
			[ 0,     	f,		0,		0],
			[ 0,        0,		a,	    1],
			[ 0,	    0,	   -b,		0]
		];

		glMatrixMode(GL_PROJECTION);
		glLoadMatrixf(cast(float*) projectionMatrix);

		// view
		float positionFactor = useFastForPosition ? factorCameraPosition : factor;
		Float4x4 cameraMatrix = lerpRotation(first.renderRotation, second.renderRotation, factor).inverse;
		Float4 cameraPosition = remapPosition(first.position, second.position, positionFactor);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0, 0, distance);
		glRotatef(-angle / PI * 180, 1, 0, 0);
		glMultMatrixf(cameraMatrix.value_ptr);
		glTranslatef(-cameraPosition.x, -cameraPosition.y, -cameraPosition.z);
	}

	private void addFigure(in Float4 position, in float size, in uint color, 
						  in float[3][] Vertices, in uint[] Indices,
						  in uint NumVertices, in uint NumIndices)
	{
		uint base = numVertices;
		foreach (uint i; 0 .. NumVertices)
		{
			immutable v = Vertices[i];
			vertices[numVertices][0] = v[0] * size + position.x;
			vertices[numVertices][1] = v[1] * size + position.y;
			vertices[numVertices][2] = v[2] * size + position.z;
			vertices[numVertices][3] = 1.0;
			colors[numVertices] = color;
			++numVertices;
		}
		foreach (uint i; 0 .. NumIndices)
		{
			indices[numIndices] = Indices[i] + base;
			++numIndices;
		}
	}
	public void addFigure(in Int4 position, in float sizeRed, in float sizeBlue, in Float3 color,
						  in float[3][] Vertices, in uint[] Indices,
						  in uint NumVertices, in uint NumIndices)
	{
		Float4 remappedPosition = remapPosition(position, position, factorObject);
		Float4 remappedCamera = remapPosition(first.position, second.position, factorCameraPosition);

		float offsetW = (remappedPosition.w - remappedCamera.w) / maxW;
		float factorW = clamp(sgn(offsetW) * pow(abs(offsetW), 0.5f), -1.0, 1.0);
		float scaleW = factorW;
		float colorW = factorW;

		addFigure(remappedPosition, lerp(sizeRed, sizeBlue, scaleW * 0.5 + 0.5), doppler(color, colorW).asUint(),
				  Vertices, Indices, NumVertices, NumIndices);
	}
	public void addCube(in Int4 position, in float sizeRed, in float sizeBlue, in Float3 color)
	{
		static immutable float[3][8] Vertices = 
		[
			[0-H, 0-H, 0-H], [0-H, 1-H, 0-H], [0-H, 1-H, 1-H], [0-H, 0-H, 1-H], 
			[1-H, 0-H, 0-H], [1-H, 1-H, 0-H], [1-H, 1-H, 1-H], [1-H, 0-H, 1-H]
		];
		static immutable uint[24] Indices = 
		[
			0,1, 0,3, 0,4, 1,2, 1,5, 3,2, 3,7, 4,5, 4,7, 2,6, 5,6, 7,6
		];
		addFigure(position, sizeRed, sizeBlue, color, Vertices, Indices, 8, 24);
	}
	public void addAngle(in Int4 position, in Int4 eye, in float sizeRed, in float sizeBlue, in Float3 color)
	{
		addCube(position, sizeRed, sizeBlue, color);
		addCube(position + Int4(eye.x, 0, 0, 0), sizeRed, sizeBlue, color);
		addCube(position + Int4(0, eye.y, 0, 0), sizeRed, sizeBlue, color);
		addCube(position + Int4(0, 0, eye.z, 0), sizeRed, sizeBlue, color);
		addCube(position + Int4(0, 0, 0, eye.w), sizeRed, sizeBlue, color);
	}
	public void addBox(in int o, in int i, in float sizeRed, in float sizeBlue, in Float3 color)
	{
		addAngle(Int4(o, o, o, o), Int4(1, 1, 1, 1), sizeRed, sizeBlue, color);
		addAngle(Int4(i, o, o, o), Int4(-1, 1, 1, 1), sizeRed, sizeBlue, color);
		addAngle(Int4(o, i, o, o), Int4(1, -1, 1, 1), sizeRed, sizeBlue, color);
		addAngle(Int4(i, i, o, o), Int4(-1, -1, 1, 1), sizeRed, sizeBlue, color);
		addAngle(Int4(o, o, i, o), Int4(1, 1, -1, 1), sizeRed, sizeBlue, color);
		addAngle(Int4(i, o, i, o), Int4(-1, 1, -1, 1), sizeRed, sizeBlue, color);
		addAngle(Int4(o, i, i, o), Int4(1, -1, -1, 1), sizeRed, sizeBlue, color);
		addAngle(Int4(i, i, i, o), Int4(-1, -1, -1, 1), sizeRed, sizeBlue, color);
		addAngle(Int4(o, o, o, i), Int4(1, 1, 1, -1), sizeRed, sizeBlue, color);
		addAngle(Int4(i, o, o, i), Int4(-1, 1, 1, -1), sizeRed, sizeBlue, color);
		addAngle(Int4(o, i, o, i), Int4(1, -1, 1, -1), sizeRed, sizeBlue, color);
		addAngle(Int4(i, i, o, i), Int4(-1, -1, 1, -1), sizeRed, sizeBlue, color);
		addAngle(Int4(o, o, i, i), Int4(1, 1, -1, -1), sizeRed, sizeBlue, color);
		addAngle(Int4(i, o, i, i), Int4(-1, 1, -1, -1), sizeRed, sizeBlue, color);
		addAngle(Int4(o, i, i, i), Int4(1, -1, -1, -1), sizeRed, sizeBlue, color);
		addAngle(Int4(i, i, i, i), Int4(-1, -1, -1, -1), sizeRed, sizeBlue, color);
	}
	public void addStar(in Int4 position, in float sizeRed, in float sizeBlue, in Float3 color)
	{
		static immutable float G = 0.3f;
		static immutable float[3][15] Vertices = 
		[
			[0, 0, 0],
			[-H, 0, 0], [H, 0, 0], [0, -H, 0], [0, H, 0], [0, 0, -H], [0, 0, H],
			[-G, -G, -G], [-G, -G, G], [-G, G, -G], [-G, G, G], [G, -G, -G], [G, -G, G], [G, G, -G], [G, G, G], 
		];
		static immutable uint[28] Indices = 
		[
			0,1, 0,2, 0,3, 0,4, 0,5, 0,6, 
			0,7, 0,8, 0,9, 0,10, 0,11, 0,12, 0,13, 0,14
		];
		addFigure(position, sizeRed, sizeBlue, color, Vertices, Indices, 15, 28);
	}
	public void drawFigures()
	{
		// draw
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		glVertexPointer(4, GL_FLOAT, float.sizeof * 4, cast(void*) vertices);
		glColorPointer(4, GL_UNSIGNED_BYTE, uint.sizeof, cast(void*) colors);

		glDrawElements(GL_LINES, numIndices, GL_UNSIGNED_INT, cast(void*) indices);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);

		// reset
		numVertices = 0;
		numIndices = 0;
	}

	private void addGrid(in Float4 from1, in Float4 to1, in Float4 from2, in Float4 to2, in uint size, in uint color)
	{
		for (uint x = 0; x <= size; ++x)
		{
			float k = x / cast(float) size;

			vertices[numVertices] = lerp(from1, from2, k).vector[0..3] ~ 1.0f;
			colors[numVertices++] = color;

			vertices[numVertices] = lerp(to1, to2, k).vector[0..3] ~ 1.0f;
			colors[numVertices++] = color;

			vertices[numVertices] = lerp(from1, to1, k).vector[0..3] ~ 1.0f;
			colors[numVertices++] = color;

			vertices[numVertices] = lerp(from2, to2, k).vector[0..3] ~ 1.0f;
			colors[numVertices++] = color;
		}
	}
	private Float4 remapVertex(in Float4 vertex)
	{
		Float4 first = vertex * castMatrix(first.remap);
		Float4 second = vertex * castMatrix(second.remap);
		return lerp(first, second, factor);
	}
	private Float3 gridColor(in Float3 color)
	{
		static immutable Float3 Red = Float3(1.0, 0.0, 0.0);
		static immutable Float3 Blue = Float3(0.0, 0.0, 1.0);

		float half = cast(float) (gridSize - 1) / 2;
		float cameraW = remapPosition(first.position, second.position, factorCameraPosition).w;
		if (cameraW <= half)
			return lerp(Red, color, clamp((cameraW/half) ^^ 2, 0.0f, 1.0f));
		else
			return lerp(color, Blue, clamp(((cameraW - half)/half) ^^ 2, 0.0f, 1.0f));
	}
	private void addGrid(in Float4 a, in Float4 b, in Float4 c, in Float4 d, in Float3 color)
	{
		float size = cast(float) gridSize;
		addGrid(Float4(-H, -H, -H, 1) + a*size, 
				Float4(-H, -H, -H, 1) + b*size,  
				Float4(-H, -H, -H, 1) + c*size,  
				Float4(-H, -H, -H, 1) + d*size, 
				gridSize, gridColor(color).asUint());
	}
	public void drawGrid(in Float3 color)
	{
		// prepare
		assert(numVertices == 0 && numIndices == 0);

		addGrid(Float4(0, 0, 0, 0), Float4(0, 0, 1, 0), Float4(1, 0, 0, 0), Float4(1, 0, 1, 0), color);
		addGrid(Float4(0, 1, 0, 0), Float4(0, 1, 1, 0), Float4(1, 1, 0, 0), Float4(1, 1, 1, 0), color);
		addGrid(Float4(0, 0, 0, 0), Float4(1, 0, 0, 0), Float4(0, 1, 0, 0), Float4(1, 1, 0, 0), color);
		addGrid(Float4(0, 0, 0, 0), Float4(0, 0, 1, 0), Float4(0, 1, 0, 0), Float4(0, 1, 1, 0), color);
		addGrid(Float4(1, 0, 0, 0), Float4(1, 0, 1, 0), Float4(1, 1, 0, 0), Float4(1, 1, 1, 0), color);
		addGrid(Float4(0, 0, 1, 0), Float4(1, 0, 1, 0), Float4(0, 1, 1, 0), Float4(1, 1, 1, 0), color);

		// draw
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		glVertexPointer(4, GL_FLOAT, float.sizeof * 4, cast(void*) vertices);
		glColorPointer(4, GL_UNSIGNED_BYTE, uint.sizeof, cast(void*) colors);

		glDrawArrays(GL_LINES, 0, numVertices);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);

		// reset
		numVertices = 0;
		numIndices = 0;
	}
	public void drawRemaning(in Float3 color, in float size)
	{
		Int4 pos = second.position;
		Int4 dir = moveDirection();
		int z = pos * dir;
		int remaning;
		if (dir.x > 0 || dir.y > 0 || dir.z > 0 || dir.w > 0)
			remaning = gridSize - z - 1;
		else
			remaning = -z;
		foreach (uint i; 1 .. remaning + 1)
		{
			addCube(pos + dir * i, size, size, color);
		}
	}

	public const Int4 moveDirection()
	{
		return getAxis(second.poseMatrix, 2);
	}
	public const Int4 colorDirection()
	{
		return getAxis(second.poseMatrix, 3);
	}
	public const Int4 cameraPosition()
	{
		return second.position;
	}

private:
	uint gridSize;
	uint maxW;
	float remapSpeed;

	struct State
	{
		Int4 position;
		Int4x4 poseMatrix = Int4x4.identity;
		Int4x4 renderRotation = Int4x4.identity;
		Int4x4 remap = Int4x4.identity;
		public void reset(Int4 position)
		{
			this.position = position;
			this.poseMatrix = Int4x4.identity;
			this.renderRotation = Int4x4.identity;
			this.remap = Int4x4.identity;
		}
	}
	State first;
	State second;
	float factor;
	float factorObject;
	float factorCameraPosition;
	bool useFastForPosition = false;

	uint numVertices;
	float[4][MaxVertices] vertices;
	uint[MaxVertices] colors;
	uint numIndices;
	uint[MaxIndices] indices;
}

