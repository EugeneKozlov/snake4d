﻿module Snake4D.main;

import std.stdio;
import std.string;
import std.math;
import std.algorithm;
import std.conv;
import core.sys.windows.windows;

import derelict.opengl3.gl;
import derelict.glfw3.glfw3;

import Snake4D.util;
import Snake4D.World4D;
import Snake4D.Snake;

static Snake game = null;
static bool paused = false;

extern (C) void onError(int error, const (char)* desc) nothrow
{
	scope(failure) assert(0);
	writeln("Houston, we've had a problem: [", error, "] ", desc);
}

extern (C) void onKey(GLFWwindow* window, int key, int scancode, int action, int mods) nothrow
{
	if (action == GLFW_PRESS)
	{
		switch (key)
		{
			case GLFW_KEY_ESCAPE:
				glfwSetWindowShouldClose(window, GL_TRUE);
				break;
			case GLFW_KEY_SPACE:
				paused = !paused;
				break;
			case GLFW_KEY_W:
				game.addDelayedAction(Action.Up);
				break;
			case GLFW_KEY_A:
				game.addDelayedAction(Action.Left);
				break;
			case GLFW_KEY_S:
				game.addDelayedAction(Action.Down);
				break;
			case GLFW_KEY_D:
				game.addDelayedAction(Action.Right);
				break;
			case GLFW_KEY_DOWN:
				game.addDelayedAction(Action.Red);
				break;
			case GLFW_KEY_UP:
				game.addDelayedAction(Action.Blue);
				break;
			case GLFW_KEY_Q:
				game.addDelayedAction(Action.Horize);
				break;
			case GLFW_KEY_E:
				game.addDelayedAction(Action.Vertize);
				break;
			case GLFW_KEY_LEFT:
				//game.addDelayedAction(Action.Past);
				break;
			case GLFW_KEY_RIGHT:
				//game.addDelayedAction(Action.Future);
				break;
			default:
				return;
		}	
	}
}

extern (C) void onResize(GLFWwindow* window, int width, int height) nothrow
{
	glViewport(0, 0, width, height);
}

void main(string[] args)
{
	try
	{
		Float3 background = Float3(0, 0, 0);
		if (args.length >= 4)
		{
			background.x = to!uint(args[1]) / 255.0f;
			background.y = to!uint(args[2]) / 255.0f;
			background.z = to!uint(args[3]) / 255.0f;
		}

		// start glfw
		writeln("Starting...");
		DerelictGLFW3.load();
		DerelictGL.load();
		glfwSetErrorCallback(&onError);
		if (!glfwInit())
			return;

		// init window
		GLFWwindow* window = glfwCreateWindow(640, 480, "Snake 4D", null, null);
		if (window == null)
			return;
		glfwSetKeyCallback(window, &onKey);
		glfwSetFramebufferSizeCallback(window, &onResize);
		glfwMakeContextCurrent(window);

		// run
		double lastTime = 0;
		while (!glfwWindowShouldClose(window))
		{
			glfwPollEvents();

			// update time
			double currentTime = glfwGetTime();
			double deltaTime = min(0.1, currentTime - lastTime);
			lastTime = currentTime;
			if (paused) 
				continue;

			// update game
			if (game !is null)
			{
				if (game.update(deltaTime))
				{
					string title = "Your score is " ~ to!string(game.score()) ~ " [Snake4D by -Eugene-]";
					glfwSetWindowTitle(window, title.toStringz);
				}
				else
				{
					string title = "Gamover! Your score is " ~ to!string(game.score());
					MessageBoxA(cast(void*) 0, title.toStringz, "Fail".toStringz, 0);
					game = null;
					continue;
				}
			}
			else
			{
				//g.game = new Snake(9, 3, 2, 2.0, 0.995);
				game = new Snake(9, 3, 1, 1.0, 0.99);
			}


			// draw
			float ratio;
			int width, height;
			glfwGetFramebufferSize(window, &width, &height);
			ratio = width / cast(float) height;
			glViewport(0, 0, width, height);
			glClearColor(background.x, background.y, background.z, 1.0);
			glClearDepth(1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			game.draw(PI / 3, 0.1f, 100.0f, ratio);
		
			glfwSwapBuffers(window);
		}

		// finish
		if (window != null)
			glfwDestroyWindow(window);
		glfwTerminate();
	}
	catch(Exception e)
	{
		writeln("Catch this: ", e.msg);
	}
}

